﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags
{
    // Player
    public const string Player = "Player";

    // Elements
    public const string Hydrogen = "Hydrogen";
    public const string Oxygen = "Oxygen";
    public const string Chlorine = "Chlorine";
    public const string Sodium = "Sodium";
    public const string Sulfide = "Sulfide";

    // Obstacles
    public const string Fire = "Fire";
    public const string MeltableBlock = "MeltableBlock";
    public const string RustedBlock = "RustedBlock";
    public const string Block = "Block";
    public const string Particle = "Particle";
    public const string Battery = "Battery";
    public const string Bridge = "Bridge";
    public const string Lever = "Lever";
    public const string RustedLever = "RustedLever";
    public const string Spikes = "Spikes";
    public const string Finish = "Finish";

    // Crafting
    public const string CraftStone = "CraftStone";
}
