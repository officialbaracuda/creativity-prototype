﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public GameObject craftingPanel;
    public GameObject notification;
    public Button naOH;
    public Button hCl;
    public Button h2O;
    public Button h2SO4;
    public Text OxygenTxt;
    public Text HydrogenTxt;
    public Text ChlorineTxt;
    public Text SodiumTxt;
    public Text SulfideTxt;

    // Start is called before the first frame update
    void Start()
    {
        craftingPanel.SetActive(false);
    }

    public void panelSetActive(bool state)
    {
        craftingPanel.SetActive(state);
    }

    public void h2OSetActive(bool state)
    {
        h2O.interactable = state;
    }

    public void naOHSetActive(bool state)
    {
        naOH.interactable = state;
    }

    public void hClSetActive(bool state)
    {
        hCl.interactable = state;
    }

    public void h2SO4SetActive(bool state)
    {
        h2SO4.interactable = state;
    }

    public void setOxygenCount(int num)
    {
        OxygenTxt.text = "Oxygen: " + num;
    }

    public void setHydrogenCount(int num)
    {
        HydrogenTxt.text = "Hydrogen: " + num;
    }

    public void setSulfideCount(int num)
    {
        SulfideTxt.text = "Sulfide: " + num;
    }

    public void setSodiumCount(int num)
    {
        SodiumTxt.text = "Sodium: " + num;
    }

    public void setChlorineCount(int num)
    {
        ChlorineTxt.text = "Chlorine: " + num;
    }

    public void notify(string text)
    {
        StartCoroutine(showNotification(text));
    }

    IEnumerator showNotification(string text)
    {
        if (text.Equals("SUCCESS"))
        {
            notification.gameObject.GetComponent<Image>().color = new Color(0, 1, 0);
        }else
        {
            notification.gameObject.GetComponent<Image>().color = new Color(0.6f, 0.14f, 0.14f);
        }
        notification.SetActive(true);
        notification.GetComponentInChildren<Text>().text = text;
        yield return new WaitForSeconds(2f);
        notification.SetActive(false);
        notification.GetComponentInChildren<Text>().text = "";

    }

    public void reset()
    {
        int level = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(level);
    }
}
