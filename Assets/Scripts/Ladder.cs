﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{

    private GameObject player;
    private bool canClimb = false;
    private float speed = 1;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            canClimb = true;
            player = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            canClimb = false;
            player = null;
        }
    }

    void Update()
    {
        if (canClimb)
        {
            if (Input.GetKey(KeyCode.W))
            {
                player.transform.Translate(new Vector3(0, 2, 0) * Time.deltaTime * speed);
            }
            if (Input.GetKey(KeyCode.S))
            {
                player.transform.Translate(new Vector3(0, -2, 0) * Time.deltaTime * speed);
            }
        }
    }
}
