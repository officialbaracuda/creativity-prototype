﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public GameObject hiddenSpikes;
    public GameObject spikes;
    void Start()
    {
        hiddenSpikes.SetActive(false);
        spikes.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(Tags.Block)) {
            hiddenSpikes.SetActive(true);
            spikes.SetActive(false);
        }
    }
}
