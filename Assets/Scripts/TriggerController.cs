﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TriggerController : MonoBehaviour
{
    private UIController uiController;
    public GameObject MoleculePanel_H2O;
    public GameObject MoleculePanel_NaOH;
    public GameObject MoleculePanel_HCl;
    public GameObject MoleculePanel_H2SO4;
    public GameObject box;
    public GameObject bridge;
    public GameObject lever;
    private Image MoleculeImage;
    [SerializeField]
    private int NoOfHydrogens = 0;
    [SerializeField]
    private int NoOfOxygens = 0;
    [SerializeField]
    private int NoOfChlorines = 0;
    [SerializeField]
    private int NoOfSodium = 0;
    [SerializeField]
    private int NoOfSulfide = 0;
    [SerializeField]
    private bool hasH2O = false;
    [SerializeField]
    private bool hasNaOH = false;
    [SerializeField]
    private bool hasHCl = false;
    [SerializeField]
    private bool hasH2SO4 = false;
    [SerializeField]
    private bool leverMove = false;

    public Material green;

    void Start()
    {
        uiController = GameObject.Find("UIController").GetComponent<UIController>();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case Tags.Oxygen: { NoOfOxygens++; uiController.setOxygenCount(NoOfOxygens); Destroy(other.gameObject); } break;
            case Tags.Hydrogen: { NoOfHydrogens++; uiController.setHydrogenCount(NoOfHydrogens); Destroy(other.gameObject); } break;
            case Tags.Sodium: { NoOfSodium++; uiController.setSodiumCount(NoOfSodium); Destroy(other.gameObject); } break;
            case Tags.Chlorine: { NoOfChlorines++; uiController.setChlorineCount(NoOfChlorines); Destroy(other.gameObject); } break;
            case Tags.Sulfide: { NoOfSulfide++; uiController.setSulfideCount(NoOfSulfide); Destroy(other.gameObject); } break;
            case Tags.CraftStone: { checkCompund(); uiController.panelSetActive(true); } break;
            case Tags.Lever: {
                    if (leverMove)
                    {
                        other.gameObject.transform.Rotate(45, 0, 0); bridge.transform.Rotate(0, 0, 45);
                        leverMove = false;
                    }
                    else {
                        uiController.notify("Battery is not powerful enough to move the bridge. You need to fill the battery with H2SO4.");
                    }
                    
                } break;
            case Tags.Spikes: { uiController.notify("The spikes seem to connected to a mechanism. You need to activate it to pass."); } break;
            case Tags.Bridge: { uiController.notify("The bridge can be lowered with the lever."); } break;
            case Tags.Finish: { uiController.notify("SUCCESS"); Time.timeScale = 0; } break;
        }

        checkCompund();
    }

    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case Tags.Fire:
                {
                    if (hasH2O)
                    {
                        Destroy(GameObject.FindGameObjectWithTag(Tags.Particle));
                        collision.gameObject.GetComponent<CapsuleCollider>().enabled = false;
                        Hide(MoleculePanel_H2O);
                        hasH2O = false;
                    }
                    else
                    {
                        uiController.notify("Fire does not let you pass. You need to extinguish it with H2O.");
                    }

                }
                break;
            case Tags.RustedBlock:
                {
                    if (hasHCl)
                    {
                        Destroy(collision.gameObject);
                        box.SetActive(true);
                        Hide(MoleculePanel_HCl);
                        hasHCl = false;
                    }
                    else
                    {
                        uiController.notify("The box does not move because of the rust. You need to clear the rust with HCl");
                    }
                }
                break;
            case Tags.MeltableBlock:
                {
                    if (hasNaOH)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("melt", true);
                        Destroy(collision.gameObject, 1f);
                        Hide(MoleculePanel_NaOH);
                        hasNaOH = false;
                    }
                    else
                    {
                        uiController.notify("The aluminum block is occluding the way. You need to melt it with NaOH.");
                    }
                }
                break;
            case Tags.RustedLever:
                {
                    if (hasHCl)
                    {
                        lever.SetActive(true);
                        Destroy(collision.gameObject, 1f);
                        Hide(MoleculePanel_HCl);
                        hasHCl = false;
                    }
                    else
                    {
                        uiController.notify("The laver seems rusted. You need to clear the rust with HCl.");
                    }
                }
                break;
            case Tags.Battery:
                {
                    if (hasH2SO4)
                    {
                        Hide(MoleculePanel_H2SO4);
                        hasH2SO4 = false;
                        leverMove = true; 
                        Material[] mats = collision.gameObject.GetComponent<MeshRenderer>().materials;
                        mats[2] = green;
                        collision.gameObject.GetComponent<MeshRenderer>().materials = mats;
                    }
                    else
                    {
                        uiController.notify("This battery is empty it needs to be filled with H2SO4.");
                    }
                }
                break;
        }

    }

    void OnTriggerExit(Collider other)
    {
        switch (other.tag)
        {
            case Tags.CraftStone: { uiController.panelSetActive(false); } break;
        }
    }

    void Show(GameObject moleculePanel)
    {
        MoleculeImage = moleculePanel.gameObject.GetComponent<Image>();
        var NewTransparency = MoleculeImage.color;
        NewTransparency.a = 1f;
        MoleculeImage.color = NewTransparency;
    }

    void Hide(GameObject moleculePanel)
    {
        MoleculeImage = moleculePanel.gameObject.GetComponent<Image>();
        var NewTransparency = MoleculeImage.color;
        NewTransparency.a = 0.1333f;
        MoleculeImage.color = NewTransparency;
    }

    public void generateCompund(string text)
    {
        switch (text)
        {
            case "h2o": { hasH2O = true; NoOfHydrogens -= 2; NoOfOxygens--; checkCompund(); Show(MoleculePanel_H2O); } break;
            case "hcl": { hasHCl = true; NoOfHydrogens--; NoOfChlorines--; checkCompund(); Show(MoleculePanel_HCl); } break;
            case "naoh": { hasNaOH = true; NoOfHydrogens--; NoOfOxygens--; NoOfSodium--; checkCompund(); Show(MoleculePanel_NaOH); } break;
            case "h2so4": { hasH2SO4 = true; NoOfHydrogens -= 2; NoOfOxygens -= 4; NoOfSulfide--; checkCompund(); Show(MoleculePanel_H2SO4); } break;
        }
        uiController.setOxygenCount(NoOfOxygens);
        uiController.setChlorineCount(NoOfChlorines);
        uiController.setHydrogenCount(NoOfHydrogens);
        uiController.setSodiumCount(NoOfSodium);
    }

    private void checkCompund()
    {
        if (NoOfSodium >= 1 && NoOfOxygens >= 1 && NoOfHydrogens >= 1)
        {
            uiController.naOHSetActive(true);
        }
        else
        {
            uiController.naOHSetActive(false);
        }

        if (NoOfOxygens >= 1 && NoOfHydrogens >= 2)
        {
            uiController.h2OSetActive(true);
        }
        else
        {
            uiController.h2OSetActive(false);
        }

        if (NoOfChlorines >= 1 && NoOfHydrogens >= 1)
        {
            uiController.hClSetActive(true);
        }
        else
        {
            uiController.hClSetActive(false);
        }

        if (NoOfOxygens >= 4 && NoOfHydrogens >= 2 && NoOfSulfide >=1)
        {
            uiController.h2SO4SetActive(true);
        }
        else
        {
            uiController.h2SO4SetActive(false);
        }
    }
}
