﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera cam;
    [SerializeField]
    private GameObject player;
    private Vector3 difference;

    void Start()
    {
        cam = GetComponent<Camera>();
        difference = transform.position - player.transform.position;
    }


    void Update()
    {
        Vector3 targetPos = new Vector3(player.transform.position.x + difference.x, player.transform.position.y + difference.y, player.transform.position.z + difference.z);
        transform.position = targetPos;
    }
}
